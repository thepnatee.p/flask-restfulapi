from .db_conn import db,ma
from flask import jsonify
import json
from .convertData import dictDecimalToInt

def select_profile_by_filter(studentid):
    sql  = " SELECT TOP 1  STATUSID as statusid , STATUS_STD as statusname ,THAINAME as displayname_th ,ENGLISHNAME as displayname_en , LEVEL_NAME as level_name , FAC_T_NAME as faculty , DEP_NAME as department "
    sql += " FROM vFIM_Student "
    sql += " WHERE (STUDENTID ='{}') ".format(studentid);

    result = db.engine.execute(sql)
    result_data = [dictDecimalToInt(dict(r)) for r in result]
    if(len(result_data) > 0):
        return result_data[0]
    else:
        return None
    # result_json = json.dumps()
    # return result_json

def select_by_filter_private(studentid):
    if(studentid):
        sql  = " SELECT TOP 1 TMPCITIZENID as id_card,STATUSID as statusid , STATUS_STD as statusname ,THAITITLE as prefix_th,THAINAME as displayname_th ,ENGLISHNAME as displayname_en , LEVEL_NAME as level_name , FAC_T_NAME as faculty , DEP_NAME as department "
        sql += " FROM vFIM_Student "
        sql += " WHERE (STUDENTID ='{}') ".format(studentid);

        result = db.engine.execute(sql)
        result_data = [dictDecimalToInt(dict(r)) for r in result]
        if(len(result_data) > 0):
            return result_data[0]
        else:
            return None
    else:
        return None


def select_fac_by_filter(filter_obj=None):
    sql  = " SELECT  FACULTYID as facultyid , FAC_T_NAME as faculty_th , FAC_ENG_NAME as faculty_en "
    sql += " FROM vStd_Fac "

    if filter_obj:
        for k, v in filter_obj.items():
            if k == "facultyid":
                sql += " WHERE {0} = '{1}' ".format(k.upper(), v)

    sql += " GROUP BY FACULTYID , FAC_T_NAME , FAC_ENG_NAME "
    result = db.engine.execute(sql);
    result_data = [dictDecimalToInt(dict(r)) for r in result]
    if(len(result_data) > 0):
        return result_data
    else:
        return None

def select_dep_by_filter(filter_obj=None):
    sql  = " SELECT  FACULTYID as facultyid ,DEP_CODE as depid , DEP_NAME as department "
    sql += " FROM vStd_Fac "
    sql += " WHERE DEP_CODE IS NOT NULL AND DEP_NAME IS NOT NULL AND DEP_NAME <> '-' "

    if filter_obj:
        for k, v in filter_obj.items():
            if k == "facultyid":
                sql += " AND ({0} = '{1}') ".format(k.upper(), v)
            elif k == "depid":
                sql += " AND ({0} = '{1}') ".format("DEP_CODE", v)

    result = db.engine.execute(sql);
    result_data = [dictDecimalToInt(dict(r)) for r in result]
    if(len(result_data) > 0):
        return result_data
    else:
        return None