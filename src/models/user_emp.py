from .db_conn import db,ma

class SYS_USER_EMPLOYEE(db.Model):
    userid = db.Column(db.Integer, primary_key=True)
    displayNameTh = db.Column(db.String(100))
    displayNameEn = db.Column(db.String(100))
    userName = db.Column(db.String(100))
    userEmail = db.Column(db.String(100))
    userType = db.Column(db.String(100))
    birthdate = db.Column(db.DateTime)
    userIDCard = db.Column(db.String(100))
    userDepartment = db.Column(db.String(500))
    userOrgCompany = db.Column(db.String(500))
    userLineuserId = db.Column(db.String(500))
    userLineaccesstoken = db.Column(db.String(500))
    userLinepictureUrl = db.Column(db.String(500))
    userLineDisplayName = db.Column(db.String(500))
    RECORD_STATUS = db.Column(db.String(1))
    CREATE_DATE = db.Column(db.DateTime)
    userStatusConnect = db.Column(db.String(1))
    eventType = db.Column(db.String(255))
    LAST_DATE = db.Column(db.DateTime)
    sha256 = db.Column(db.String(1000))
    userAdminType = db.Column(db.String(1))
    employee_type = db.Column(db.String(100))
    Status = db.Column(db.String(100))
    Level = db.Column(db.String(100))
    EMP_ID = db.Column(db.Integer)
    # content = db.Column(db.Text, nullable=False)
    # author = db.Column(db.String(20), nullable=False, default='N/A')
    # date_posted = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

    def __repr__(self):
        return str(self.userid) +" : "+ self.userName


class SYS_USER_EMPLOYEESchema(ma.Schema):
  class Meta:
    fields = ('displayNameTh', 'displayNameEn', 'Status', 'userEmail', 'userDepartment')

# Init schema
user_schema = SYS_USER_EMPLOYEESchema()
users_schema = SYS_USER_EMPLOYEESchema(many=True)