from .db_conn import db, ma
from flask import jsonify
import json


def select_std_fac_filter(filter_obj=None):
    sql = " SELECT faculty_name_th,faculty_name_en,counts FROM VW_DAS_STD_FAC_COUNT "
    sql += " WHERE faculty_name_th IS NOT NULL "

    if filter_obj:
        for k, v in filter_obj.items():
            if k in ["faculty_name_th", "faculty_name_en"]:
                sql += " AND ({0} LIKE '%{1}%') ".format(k, v)

    sql += " ORDER BY counts DESC "
    print(sql)
    result = db.engine.execute(sql)
    result_data = [dict(r) for r in result]

    if(len(result_data) > 0):
        return result_data
    else:
        return None


def select_std_level_filter(filter_obj=None):
    sql = " SELECT level_name,counts FROM VW_DAS_STD_LEVEL_NAME ORDER BY counts DESC"
    result = db.engine.execute(sql)
    result_data = [dict(r) for r in result]

    if(len(result_data) > 0):
        return result_data
    else:
        return None


def select_std_status_filter(filter_obj=None):
    sql = " SELECT status_name,counts FROM VW_DAS_STD_STATUS_COUNT ORDER BY counts DESC"
    result = db.engine.execute(sql)
    result_data = [dict(r) for r in result]

    if(len(result_data) > 0):
        return result_data
    else:
        return None


def select_std_adyear_filter(filter_obj=None):
    sql = " SELECT adyaer,counts FROM VW_DAS_STD_ADYEAR_COUNT ORDER BY counts DESC"
    result = db.engine.execute(sql)
    result_data = [dict(r) for r in result]

    if(len(result_data) > 0):
        return result_data
    else:
        return None


def select_std_bucket_filter(filter_obj=None):
    sql = " SELECT faculty_name_th,faculty_name_en,adyear,level_name,counts FROM VW_DAS_STD_BUCKET_COUNT "
    sql += " WHERE faculty_name_th IS NOT NULL "

    if filter_obj:
        for k, v in filter_obj.items():
            if k in ["faculty_name_th", "faculty_name_en", "adyear", "level_name"]:
                sql += " AND ({0} LIKE '%{1}%') ".format(k, v)

    sql += " ORDER BY counts DESC "
    print(sql)
    result = db.engine.execute(sql)
    result_data = [dict(r) for r in result]

    if(len(result_data) > 0):
        return result_data
    else:
        return None

def select_emp_org_filter(filter_obj=None):
    sql = " SELECT vFIM_Employee.ORG_NAME as Organization, COUNT(vFIM_Employee.USERNAME) as counts  "
    sql += " FROM vFIM_Employee "
    sql += " WHERE Organization IS NOT NULL AND vFIM_Employee.StatusEmp = 'ปกติ' "

    if filter_obj:
        for k, v in filter_obj.items():
            if k in ["Organization"]:
                sql += " AND ({0} LIKE '%{1}%') ".format(k, v)

    sql += " GROUP BY vFIM_Employee.ORG_NAME "
    sql += " ORDER BY counts DESC "
    print(sql)
    result = db.engine.execute(sql)
    result_data = [dict(r) for r in result]

    if(len(result_data) > 0):
        return result_data
    else:
        return None
