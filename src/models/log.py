from .db_conn import db,ma
from .std import select_by_filter_private
from datetime import datetime

class SYS_LOG(db.Model):
  logID = db.Column(db.Integer, primary_key=True)
  logIp = db.Column(db.String(255))
  logPage = db.Column(db.String(255))
  logDescription = db.Column(db.String(255))
  logStaus = db.Column(db.String(255))
  tokenKey = db.Column(db.String(500))
  logMethodName = db.Column(db.String(500))
  RECORD_STATUS = db.Column(db.String(1))
  CREATE_DATE = db.Column(db.DateTime)
  LAST_DATE = db.Column(db.DateTime)

  def __init__(self, logIp, logPage, logDescription, logStaus,tokenKey,RECORD_STATUS,CREATE_DATE,LAST_DATE,logMethodName):
    self.logIp = logIp
    self.logPage = logPage
    self.logDescription = logDescription
    self.logStaus = logStaus
    self.tokenKey = tokenKey
    self.RECORD_STATUS = RECORD_STATUS
    self.CREATE_DATE = CREATE_DATE
    self.LAST_DATE = LAST_DATE
    self.logMethodName = logMethodName

class SYS_LOG_PROFILE_STD(db.Model):
  userLogId = db.Column(db.Integer, primary_key=True)
  userName = db.Column(db.String(200))
  userPrefixName = db.Column(db.String(200))
  userDisplaynameTh = db.Column(db.String(200))
  userDisplaynameEn = db.Column(db.String(200))
  userIDCard = db.Column(db.String(100))
  userBirthDate = db.Column(db.DateTime)
  userType = db.Column(db.String(20))
  userDepartment = db.Column(db.String(200))
  userOrganization = db.Column(db.String(200))
  tokenKey = db.Column(db.String(500))
  RECORD_STATUS = db.Column(db.String(1))
  CREATE_DATE = db.Column(db.DateTime)
  LAST_DATE = db.Column(db.DateTime)

  def __init__(self, userName, userPrefixName, userDisplaynameTh,
          userDisplaynameEn,userIDCard,userBirthDate,userType,userDepartment,
          userOrganization,tokenKey,RECORD_STATUS,CREATE_DATE,LAST_DATE):

    self.userName = userName
    self.userPrefixName = userPrefixName
    self.userDisplaynameTh = userDisplaynameTh
    self.userDisplaynameEn = userDisplaynameEn
    self.userIDCard = userIDCard
    self.userBirthDate = userBirthDate
    self.userType = userType
    self.userDepartment = userDepartment
    self.userOrganization = userOrganization
    self.tokenKey = tokenKey
    self.RECORD_STATUS = RECORD_STATUS
    self.CREATE_DATE = CREATE_DATE
    self.LAST_DATE = LAST_DATE

def add_log(log_obj):
  dateTimeObj = datetime.now()

  # logIp = log_obj.get('logIp')
  logIp = "v2"
  logPage = log_obj.get('logPage')
  logDescription = log_obj.get('logDescription')
  logStaus = log_obj.get('logStaus')
  tokenKey = log_obj.get('tokenKey')
  RECORD_STATUS = 'N'
  CREATE_DATE = dateTimeObj
  LAST_DATE = dateTimeObj
  logMethodName = log_obj.get('logMethodName')

  new_log = SYS_LOG(logIp, logPage[0:200], logDescription, logStaus,tokenKey,RECORD_STATUS,CREATE_DATE,LAST_DATE,logMethodName)

  db.session.add(new_log)
  db.session.commit()

  # return log_schema.dump(new_log)
  return 'pass'

def add_logProfile(log_obj,id = None,key = None):
  dateTimeObj = datetime.now()

  ds = select_by_filter_private(id)

  if(ds):
    userName = id
    userPrefixName = ds.get('prefix_th')
    userDisplaynameTh = ds.get('displayname_th')
    userDisplaynameEn = ds.get('displayname_en')
    userIDCard = ds.get('id_card')
    userBirthDate = ds.get('birthdate')
    userType = 'student'
    userDepartment = ds.get('department')
    userOrganization = ds.get('faculty')
    tokenKey = key
    RECORD_STATUS = 'N'
    CREATE_DATE = dateTimeObj
    LAST_DATE = dateTimeObj

    new_log = SYS_LOG_PROFILE_STD(userName, userPrefixName, userDisplaynameTh,
            userDisplaynameEn,userIDCard,userBirthDate,userType,userDepartment,
            userOrganization,tokenKey,RECORD_STATUS,CREATE_DATE,LAST_DATE)

    db.session.add(new_log)
    db.session.commit()
    return 'pass'
  else:
    return None

def select_by_filter(filter_obj=None):
    sql  = " SELECT logDescription as Description,logStaus as Status,CREATE_DATE as CreateDate "
    sql += " FROM VW_SYS_LOG WHERE (RECORD_STATUS='N')"

    if filter_obj:
        for k, v in filter_obj.items():
            if k in ["tokenKey","logStaus","logMethodName","logDescription","convert_date_create_date"]:
                if k in ["tokenKey","logMethodName","convert_date_create_date"]:
                    sql += " AND ({0} = '{1}') ".format(k, v)
                else:
                    sql += " AND ({0} LIKE '%{1}%') ".format(k, v)
        
        if filter_obj.get("IN_SQL"):
          sql += " AND ( "+filter_obj.get("IN_SQL")+" ) "

        if filter_obj.get("IN_ID"):
          sql += " AND ( logID IN ("+filter_obj.get("IN_ID")+") ) "

        if filter_obj.get("NOT_IN_ID"):
          sql += " AND ( logID NOT IN ("+filter_obj.get("NOT_IN_ID")+") ) "

    sql += " ORDER BY CREATE_DATE DESC "
    result = db.engine.execute(sql);
    result_data = [dictDecimalToInt(dict(r)) for r in result]
    if(len(result_data) > 0):
        return result_data
    else:
        return None