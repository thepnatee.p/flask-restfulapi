from .db_conn import db,ma

class ORG_FLOW(db.Model):
    flowID = db.Column(db.Integer, primary_key=True)
    flowName = db.Column(db.String(255))
    flowDescription = db.Column(db.String(500))
    token = db.Column(db.String(500))
    flowDataJson = db.Column(db.Text)
    RECORD_STATUS = db.Column(db.String(1))
    CREATE_DATE = db.Column(db.DateTime)
    flowCode = db.Column(db.String(500))

    # def __repr__(self):
    #     return self.flowDataJson


def filter_current(obj_data,obj_filter):
    for key_filter, value_filter in obj_filter.items():
        if (obj_data.get(key_filter) == value_filter):
            return obj_data
        else:
            if(obj_data.get("children")):
                for data_children in obj_data.get("children"):
                    filter_children = filter_current(data_children,obj_filter)
                    if(filter_children):
                        return filter_children
            else:
                return None

def filter_parent(obj_data,data_children):
    if(obj_data.get("children")):
        if data_children in obj_data.get("children"):
            return obj_data;
        else:
            for flow_children in obj_data.get("children"):
                filter_children = filter_parent(flow_children,data_children)
                if(filter_children):
                    return filter_children
    else:
        return None

def select_flow_by_filter(obj_data,username):
    obj_filter = {'username':username}
    # print(obj_data)
    # print(obj_filter)
    filter_data = filter_current(obj_data,obj_filter)
    data_parent = filter_parent(obj_data,filter_data)
    result_data = None

    if(data_parent):
        del data_parent["children"]

    if(filter_data):
        result_data = {
            'Level': filter_data.get("Level"), 
            'username': filter_data.get("username"), 
            'header': data_parent,
            'children':filter_data.get("children")
        }

    return result_data
