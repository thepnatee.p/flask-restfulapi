from .db_conn import db,ma
from flask import jsonify
import json

def select_profile_by_filter(username):
    sql  = " SELECT TOP 1 USERNAME as username,PFIX_NAME as prefix,EMP_NAME as displayname_th ,ENGLISH_NAME as displayname_en , EMP_TYPE_NAME as employee_type,StatusEmp as statusname  , ORG_NAME as organization , DEP_NAME as department "
    sql += " FROM vFIM_Employee "
    sql += " WHERE (USERNAME ='{}') ".format(username);

    result = db.engine.execute(sql);
    result_data = [dict(r) for r in result]
    if(len(result_data) > 0):
        return result_data[0]
    else:
        return None

def select_org_by_filter(filter_obj=None):
    sql  = " SELECT org_code,org_name_th,org_name_en "
    sql += " FROM VW_EMP_ORG "
    sql += " WHERE org_code IS NOT NULL "

    if filter_obj:
        for k, v in filter_obj.items():
            if k in ["org_code","org_name_th","org_name_en"]:
                if k in ["org_code"]:
                    sql += " AND ({0} = '{1}') ".format(k, v)
                else:
                    sql += " AND ({0} LIKE '%{1}%') ".format(k, v)

    print(sql)
    result = db.engine.execute(sql);
    result_data = [dict(r) for r in result]

    if(len(result_data) > 0):
        return result_data
    else:
        return None

    

def select_dep_by_filter(filter_obj=None):
    sql  = " SELECT   org_code, org_name_th,   org_name_en,  dep_code,  dep_name "
    sql += " FROM VW_EMP_DEP "
    sql += " WHERE org_code IS NOT NULL "

    if filter_obj:
        for k, v in filter_obj.items():
            if k in ["org_code","dep_code","org_name_th","org_name_en","dep_name"]:
                if k in ["org_code","dep_code"]:
                    sql += " AND ({0} = '{1}') ".format(k, v)
                else:
                    sql += " AND ({0} LIKE '%{1}%') ".format(k, v)

    print(sql)
    result = db.engine.execute(sql)
    result_data = [dict(r) for r in result]
    if(len(result_data) > 0):
        return result_data
    else:
        return None

def select_user_by_filter(filter_obj=None):
    sql  = " SELECT org_code,org_name_th,org_name_en "
    sql += " FROM TU_LINE_VW_UNION_ALL_USER "
    sql += " WHERE org_code IS NOT NULL "

    if filter_obj:
        for k, v in filter_obj.items():
            if k in ["org_code","org_name_th","org_name_en"]:
                if k in ["org_code"]:
                    sql += " AND ({0} = '{1}') ".format(k, v)
                else:
                    sql += " AND ({0} LIKE '%{1}%') ".format(k, v)

    print(sql)
    result = db.engine.execute(sql);
    result_data = [dict(r) for r in result]

    if(len(result_data) > 0):
        return result_data
    else:
        return None

    