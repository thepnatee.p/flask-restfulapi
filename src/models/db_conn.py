import urllib

from flask import current_app
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow 

# Localhost
# params = urllib.parse.quote_plus("DRIVER={SQL Server};Server=localhost;Database=flask_api;UID=sa;PWD=M55qlP@55w0rd;") 
# params = urllib.parse.quote_plus("DRIVER={SQL Server};Server=localhost;Database=TU_API;UID=sa;PWD=P@55w0rd;") 

# Windows Server
# params = urllib.parse.quote_plus("DRIVER={SQL Server};Server=10.12.112.199;Database=TU_API;UID=siwakorn;PWD=Seankhram;")
# params = urllib.parse.quote_plus("DRIVER={SQL Server};Server=10.12.112.199;Database=TU_API;UID=sup_pra;PWD=SQL$3rv3r;")

# Docker Server 
params = urllib.parse.quote_plus("DRIVER={FreeTDS};Server=10.12.112.199;PORT=1433;Database=TU_API;UID=siwakorn;PWD=Seankhram;TDS_Version=8.0;")

current_app.config['SQLALCHEMY_DATABASE_URI'] = "mssql+pyodbc:///?odbc_connect=%s" % params
current_app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(current_app)
ma = Marshmallow(current_app)