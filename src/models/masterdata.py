import json,os

basedir = os.path.abspath(os.path.dirname(__file__))

def select_map_thai_by_filter(filter_obj=None):
    data_file = os.path.join(basedir, 'json/map_thai.json')
    read_file = open(data_file,encoding="utf8") 
    filter_data = json.load(read_file) 

    if(filter_obj.get("sub_district")):
        filter_data = list(filter(lambda data: data['sub_district'] == filter_obj.get("sub_district"), filter_data))
    
    if(filter_obj.get("district")):
        filter_data = list(filter(lambda data: data['district'] == filter_obj.get("district"), filter_data))

    if(filter_obj.get("province")):
        filter_data = list(filter(lambda data: data['province'] == filter_obj.get("province"), filter_data))

    if(filter_obj.get("zipcode")):
        filter_data = list(filter(lambda data: data['zipcode'] == int(filter_obj.get("zipcode")), filter_data))

    if(filter_obj.get("sub_district_code")):
        filter_data = list(filter(lambda data: data['sub_district_code'] == int(filter_obj.get("sub_district_code")), filter_data))

    if(filter_obj.get("district_code")):
        filter_data = list(filter(lambda data: data['district_code'] == int(filter_obj.get("district_code")), filter_data))

    if(filter_obj.get("province_code")):
        filter_data = list(filter(lambda data: data['province_code'] == int(filter_obj.get("province_code")), filter_data))

    read_file.close() 

    return filter_data