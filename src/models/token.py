from .db_conn import db,ma

class SYS_TOKEN_KEY(db.Model):
    tokenID = db.Column(db.Integer, primary_key=True)
    tokenCode = db.Column(db.String(500))
    tokenKey = db.Column(db.String(500))
    tokenName = db.Column(db.String(100))
    tokenDescription = db.Column(db.String(300))
    tokenWebhookStatus = db.Column(db.String(1))
    tokenWebhookUrl = db.Column(db.String(500))
    RECORD_STATUS = db.Column(db.String(1))
    CREATE_DATE = db.Column(db.DateTime)
    CREATE_USER = db.Column(db.String(50))
    LAST_DATE = db.Column(db.DateTime)
    LAST_USER = db.Column(db.String(50))
    tokenClientType = db.Column(db.String(20))
    tokenAdminApprovedCredentials = db.Column(db.String(50))
    tokenReportStatus = db.Column(db.String(1))
