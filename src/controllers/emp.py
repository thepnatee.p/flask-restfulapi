from flask import Blueprint,current_app,request,jsonify
from ..models import  token,log,emp
import time
import json

emp_bp = Blueprint("emp", __name__)

# path : /api/v2/emp/org/all/
# Method : GET
# Description : Get all Organization
# response : 
# {
#     "timestamp": 1580800196456,
#     "status": true,
#     "message": "Success",
#     "data": [
#         {
#             "org_code": "1040000",
#             "org_name_th": "กองกิจการนักศึกษา",
#             "org_name_en": "StudentAffairs"
#         },
#         {
#             "org_code": "1080000",
#             "org_name_th": "กองบริหารศูนย์รังสิต",
#             "org_name_en": "RangsitCenter"
#         },...
#     ]
# }

@emp_bp.route("/org/all/")
def org_all():

    # รับค่า header จาก request
    header_obj = request.headers

    #  silent=True = ให้ไม่ต้องแสดง error จาก การ get data json ได้
    #body_obj = request.get_json(silent=True)

    # ประกาศ Object สำหรับเก็บค่า log
    dt = dict()

    # Check Application-Key ต้องไม่เท่ากับค่าว่าง
    if header_obj.get("Application-Key"):
        dt['tokenKey'] = header_obj.get("Application-Key")
        dt['logPage'] = request.path
        dt['logMethodName'] = 'Organization All'

        # เตรียมข้อมูลที่ขอ Token Key
        filter_token = {
            "tokenKey":header_obj["Application-Key"],
            "tokenWebhookStatus":"E",
            "tokenClientType":"Public"
        }
        tokenCheck = token.SYS_TOKEN_KEY.query.filter_by(**filter_token).first()

        # check tokenKey ต้องมีใน Database
        if tokenCheck:

            data_response = emp.select_org_by_filter()
            timestampStr = int(time.time() * 1000)

            if data_response:
                dt['logDescription'] = 'Get Data Organization All Success!'
                dt['logStaus'] = 'TRUE : 200 Method:' + request.method + '/Organization All'
                
                log.add_log(dt)
                resp_obj = {
                    "timestamp": timestampStr,
                    "status": True,
                    "message": "Success",
                    "data": data_response
                }
                return jsonify(resp_obj)

            else:
                response = {
                    "timestamp": timestampStr,
                    "status": False,
                    "message": "Get Data Organization is Empty!",
                    "data": None
                }
                return jsonify(response)

        else:
            response = {
                'status':False,
                'message':'Application-Key failed due to the following reason: invalid token. Confirm that the access token in the authorization header is valid.'
            }
            return jsonify(response), 403
    else:
        response = {
            'status':False,
            'message':'Application-Key header required. Must follow the scheme, [Application-Key: <ACCESS TOKEN>]'
        }
        return jsonify(response), 401

# path : /api/v2/emp/org/find?org_code={org_code}
# Method : GET
# Reqest : {org_code:XXXX,org_name_th:YYYY,org_name_en:ZZZZ}
# Description : Get Organization by org_code, org_name_th, org_name_en
# response : 
# {
#     "timestamp": 1580800435742,
#     "status": true,
#     "message": "Success",
#     "data": [
#         {
#             "org_code": "1090000",
#             "org_name_th": "กองอาคารสถานที่",
#             "org_name_en": "TUBuilding"
#         }
#     ]
# }

@emp_bp.route("/org/find/")
def org_find():

    # รับค่า header จาก request
    header_obj = request.headers

    # Get request 'username'
    params_obj = request.args.to_dict()

    # ประกาศ Object สำหรับเก็บค่า log
    dt = dict()

    # Check Application-Key ต้องไม่เท่ากับค่าว่าง
    if header_obj.get("Application-Key"):
        dt['tokenKey'] = header_obj.get("Application-Key")
        dt['logPage'] = request.full_path
        dt['logMethodName'] = 'Organization By filter'

        # เตรียมข้อมูลที่ขอ Token Key
        filter_token = {
            "tokenKey":header_obj["Application-Key"],
            "tokenWebhookStatus":"E",
            "tokenClientType":"Public"
        }
        tokenCheck = token.SYS_TOKEN_KEY.query.filter_by(**filter_token).first()

        # check tokenKey ต้องมีใน Database
        if tokenCheck:
            # Check Application-Key ต้องไม่เท่ากับค่าว่าง
            # ตัวสอบค่าที่ get มาต้องเป็น username 
            if params_obj and \
                (params_obj.get("org_code") != None or \
                params_obj.get("org_name_th") != None or \
                params_obj.get("org_name_en") != None):
                # ถ้ามีข้อมูลให้ส่งเข้า db
                data_response = emp.select_org_by_filter(params_obj)
                timestampStr = int(time.time() * 1000)

                if data_response:
                    dt['logDescription'] = 'Get Data Organization By filter Success'
                    dt['logStaus'] = 'TRUE : 200 Method:' + request.method + '/Organization By filter'
                    
                    log.add_log(dt)
                    resp_obj = {
                        "timestamp": timestampStr,
                        "status": True,
                        "message": "Success",
                        "data": data_response
                    }
                    return jsonify(resp_obj)

                else:
                    response = {
                        "timestamp": timestampStr,
                        'status':False,
                        'message':'Get Data Organization is Empty!',
                        "data": None
                    }
                    return jsonify(response)
            else:
                #กรณีส่งค่า username
                response = {
                    'status':False,
                    'message':'Organization Invalid!. Must follow the scheme,"/find/?org_code={org code}"'
                }
                return jsonify(response), 400

        else:
            response = {
                'status':False,
                'message':'Application-Key failed due to the following reason: invalid token. Confirm that the access token in the authorization header is valid.'
            }
            return jsonify(response), 403

    else:
        response = {
            'status':False,
            'message':'Application-Key header required. Must follow the scheme, [Application-Key: <ACCESS TOKEN>]'
        }
        return jsonify(response), 401

# path : /api/v2/emp/dep/all/
# Method : GET
# Description : Get all Department Employee
# response : 
# {
#     "timestamp": 1580800196456,
#     "status": true,
#     "message": "Success",
#     "data": [
#         {
#             "org_code": "1020000",
#             "org_name_th": "กองบริหารศูนย์ท่าพระจันทร์",
#             "org_name_en": "ThaprajanCenter",
#             "dep_code": "1020100",
#             "dep_name": "งานบริหารสำนักงาน ท่าพระจันทร์"
#         },
#         {
#             "org_code": "1020000",
#             "org_name_th": "กองบริหารศูนย์ท่าพระจันทร์",
#             "org_name_en": "ThaprajanCenter",
#             "dep_code": "1020200",
#             "dep_name": "งานประชุม"
#         },...
#     ]
# }

@emp_bp.route("/dep/all/")
def dep_all():

    # รับค่า header จาก request
    header_obj = request.headers

    # ประกาศ Object สำหรับเก็บค่า log
    dt = dict()

    # Check Application-Key ต้องไม่เท่ากับค่าว่าง
    if header_obj.get("Application-Key"):
        dt['tokenKey'] = header_obj.get("Application-Key")
        dt['logPage'] = request.path
        dt['logMethodName'] = 'Department Employee All'

        # เตรียมข้อมูลที่ขอ Token Key
        filter_token = {
            "tokenKey":header_obj["Application-Key"],
            "tokenWebhookStatus":"E",
            "tokenClientType":"Public"
        }
        tokenCheck = token.SYS_TOKEN_KEY.query.filter_by(**filter_token).first()

        # check tokenKey ต้องมีใน Database
        if tokenCheck is None:
            response = {
                'status':False,
                'message':'Application-Key failed due to the following reason: invalid token. Confirm that the access token in the authorization header is valid.'
            }
            return jsonify(response), 403

        # Check Application-Key ต้องไม่เท่ากับค่าว่าง

        data_response = emp.select_dep_by_filter()
        timestampStr = int(time.time() * 1000)

        if data_response:
            dt['logDescription'] = 'Get Data Department Employee All Success!'
            dt['logStaus'] = 'TRUE : 200 Method:' + request.method + '/Department Employee All'
            log.add_log(dt)

            resp_obj = {
                "timestamp": timestampStr,
                "status": True,
                "message": "Success",
                "data": data_response
            }
            return jsonify(resp_obj)

        else:
            response = {
                "timestamp": timestampStr,
                "status": False,
                "message": "Get Data Department Employee is Empty!",
                "data": None
            }
            return jsonify(response)
    else:
        response = {
            'status':False,
            'message':'Application-Key header required. Must follow the scheme, [Application-Key: <ACCESS TOKEN>]'
        }
        return jsonify(response), 401

# path : /api/v2/emp/dep/find?org_code={org_code}
# Method : GET
# Reqest : {org_code:XXXX,org_name_th:YYYY,org_name_en:ZZZZ}
# Description : Get Department Employee by org_code, org_name_th, org_name_en
# response : 
# {
#     "timestamp": 1580800435742,
#     "status": true,
#     "message": "Success",
#     "data": [
#         {
#             "org_code": "1020000",
#             "org_name_th": "กองบริหารศูนย์ท่าพระจันทร์",
#             "org_name_en": "ThaprajanCenter",
#             "dep_code": "1020100",
#             "dep_name": "งานบริหารสำนักงาน ท่าพระจันทร์"
#         }
#     ]
# }

@emp_bp.route("/dep/find/")
def dep_find():

    # รับค่า header จาก request
    header_obj = request.headers

    # Get request 'username'
    params_obj = request.args.to_dict()

    # ประกาศ Object สำหรับเก็บค่า log
    dt = dict()

    # Check Application-Key ต้องไม่เท่ากับค่าว่าง
    if header_obj.get("Application-Key"):
        dt['tokenKey'] = header_obj.get("Application-Key")
        dt['logPage'] = request.full_path
        dt['logMethodName'] = 'Department Employee By filter'

        # เตรียมข้อมูลที่ขอ Token Key
        filter_token = {
            "tokenKey":header_obj["Application-Key"],
            "tokenWebhookStatus":"E",
            "tokenClientType":"Public"
        }

        tokenCheck = token.SYS_TOKEN_KEY.query.filter_by(**filter_token).first()

        # check tokenKey ต้องมีใน Database
        if tokenCheck:          
            # ตัวสอบค่าที่ get มาต้องเป็น org_code, dep_code, dep_name, org_name_th, org_name_en อย่างใดอย่างหนึ่ง
            if params_obj and \
                (params_obj.get("org_code") != None or \
                params_obj.get("dep_code") != None or \
                params_obj.get("dep_name") != None or \
                params_obj.get("org_name_th") != None or \
                params_obj.get("org_name_en") != None):
                # ถ้ามีข้อมูลให้ส่งเข้า db
                data_response = emp.select_dep_by_filter(params_obj)
                timestampStr = int(time.time() * 1000)

                if data_response:
                    dt['logDescription'] = 'Get Data Department Employee By filter Success'
                    dt['logStaus'] = 'TRUE : 200 Method:' + request.method + '/Department Employee By filter'
                    
                    log.add_log(dt)
                    resp_obj = {
                        "timestamp": timestampStr,
                        "status": True,
                        "message": "Success",
                        "data": data_response
                    }
                    return jsonify(resp_obj)

                else:
                    response = {
                        "timestamp": timestampStr,
                        'status':False,
                        'message':'Get Data Department Employee is Empty!',
                        "data": None
                    }
                    return jsonify(response)
            else:
                #กรณีส่งค่า username
                response = {
                    'status':False,
                    'message':'Department Employee Invalid!. Must follow the scheme,"/find/?dep_code={dep_code}"'
                }
                return jsonify(response), 400
        else:
            response = {
                'status':False,
                'message':'Application-Key failed due to the following reason: invalid token. Confirm that the access token in the authorization header is valid.'
            }
            return jsonify(response), 403
    else:
        response = {
            'status':False,
            'message':'Application-Key header required. Must follow the scheme, [Application-Key: <ACCESS TOKEN>]'
        }
        return jsonify(response), 401

# @emp_bp.route("/allemp/")
# def allemp_find():

#     # รับค่า header จาก request
#     header_obj = request.headers

#     # Get request 'username'
#     params_obj = request.args.to_dict()

#     # ประกาศ Object สำหรับเก็บค่า log
#     dt = dict()

#     # Check Application-Key ต้องไม่เท่ากับค่าว่าง
#     if header_obj.get("Application-Key"):
#         dt['tokenKey'] = header_obj.get("Application-Key")
#         dt['logPage'] = request.full_path
#         dt['logMethodName'] = 'Organization By filter'

#         # เตรียมข้อมูลที่ขอ Token Key
#         filter_token = {
#             "tokenKey":header_obj["Application-Key"],
#             "tokenWebhookStatus":"E",
#             "tokenClientType":"Public"
#         }
#         tokenCheck = token.SYS_TOKEN_KEY.query.filter_by(**filter_token).first()

#         # check tokenKey ต้องมีใน Database
#         if tokenCheck:
#             data_response = emp.select_user_by_filter(params_obj)
#             timestampStr = int(time.time() * 1000)

#             if data_response:
#                 dt['logDescription'] = 'Get Data Organization By filter Success'
#                 dt['logStaus'] = 'TRUE : 200 Method:' + request.method + '/User Employee By filter'
                
#                 log.add_log(dt)
#                 resp_obj = {
#                     "timestamp": timestampStr,
#                     "status": True,
#                     "message": "Success",
#                     "data": data_response
#                 }
#                 return jsonify(resp_obj)

#             else:
#                 response = {
#                     "timestamp": timestampStr,
#                     'status':False,
#                     'message':'Get Data Organization is Empty!',
#                     "data": None
#                 }
#                 return jsonify(response)

#         else:
#             response = {
#                 'status':False,
#                 'message':'Application-Key failed due to the following reason: invalid token. Confirm that the access token in the authorization header is valid.'
#             }
#             return jsonify(response), 403

#     else:
#         response = {
#             'status':False,
#             'message':'Application-Key header required. Must follow the scheme, [Application-Key: <ACCESS TOKEN>]'
#         }
#         return jsonify(response), 401
