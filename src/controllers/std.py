from flask import Blueprint,current_app,request,jsonify
from ..models import  token,log,std
import time
import json

std_bp = Blueprint("std", __name__)

# path : /api/v2/std/fac/all/
# Method : GET
# Description : Get all Faculty Student
# response : 
# {
#     "timestamp": 1580800196456,
#     "status": true,
#     "message": "Success",
#     "data": [
#         {
#             "facultyid": "0",
#             "faculty_th": "วิทยาลัยพัฒนศาสตร์ ป๋วย อึ๊งภากรณ์",
#             "faculty_en": "Puey  Ungphakorn  School  of Development  Studies"
#         },
#         {
#             "facultyid": "1",
#             "faculty_th": "คณะนิติศาสตร์",
#             "faculty_en": "Faculty of Law"
#         },...
#     ]
# }

@std_bp.route("/fac/all/")
def fac_all():

    # รับค่า header จาก request
    header_obj = request.headers

    # ประกาศ Object สำหรับเก็บค่า log
    dt = dict()

    #stap 1 check Application-Key ต้องไม่เท่ากับค่าว่าง
    if header_obj.get("Application-Key"):
        dt['tokenKey'] = header_obj.get("Application-Key")
        dt['logPage'] = request.path
        dt['logMethodName'] = 'Faculty Student All'

        # เตรียมข้อมูลที่ขอ Token Key
        filter_token = {
            "tokenKey":header_obj["Application-Key"],
            "tokenWebhookStatus":"E",
            "tokenClientType":"Public"
        }

        tokenCheck = token.SYS_TOKEN_KEY.query.filter_by(**filter_token).first()

        #stap 2 check tokenKey ต้องมีใน Database
        if tokenCheck is None:
            response = {
                'status':False,
                'message':'Application-Key failed due to the following reason: invalid token. Confirm that the access token in the authorization header is valid.'
            }
            return jsonify(response), 403

        #step 3 ค้นหา Fac DB
        data_response = std.select_fac_by_filter()
        # Get Timestamp
        timestampStr = int(time.time() * 1000)

        if  data_response:
            dt['logDescription'] = 'Get Data Faculty Student All Success!'
            dt['logStaus'] = 'TRUE : 200 Method:' + request.method + '/Faculty Student All'
            
            log.add_log(dt)
            resp_obj = {
                "timestamp": timestampStr,
                "status": True,
                "message": "Success",
                "data": data_response
            }
            return jsonify(resp_obj)

        else:            
            response = {
                "timestamp": timestampStr,
                "status": False,
                "message": "Get Data Faculty Student is Empty!",
                "data": None
            }
            return jsonify(response)
    else:
        # กรณีไม่ส่ง Token มา
        response = {
            'status':False,
            'message':'Application-Key header required. Must follow the scheme, [Application-Key: <ACCESS TOKEN>]'
        }
        return jsonify(response), 401

# path : /api/v2/std/fac/find?facultyid={facultyid}
# Method : GET
# Reqest : {facultyid:XXXX}
# Description : Get Department Student by facultyid
# response : 
# {
#     "timestamp": 1580800435742,
#     "status": true,
#     "message": "Success",
#     "data": [
#         {
#             "facultyid": "0",
#             "faculty_th": "วิทยาลัยพัฒนศาสตร์ ป๋วย อึ๊งภากรณ์",
#             "faculty_en": "Puey  Ungphakorn  School  of Development  Studies"
#         }
#     ]
# }

@std_bp.route("/fac/find/")
def fac_find():

    # รับค่า header จาก request
    header_obj = request.headers

    # Get request 'username'
    params_obj = request.args.to_dict()

    # ประกาศ Object สำหรับเก็บค่า log
    dt = dict()

    #stap 1 check Application-Key ต้องไม่เท่ากับค่าว่าง
    if header_obj.get("Application-Key"):
        dt['tokenKey'] = header_obj.get("Application-Key")
        dt['logPage'] = request.full_path
        dt['logMethodName'] = 'Faculty Student By filter'

        # เตรียมข้อมูลที่ขอ Token Key
        filter_token = {
            "tokenKey":header_obj["Application-Key"],
            "tokenWebhookStatus":"E",
            "tokenClientType":"Public"
        }
        tokenCheck = token.SYS_TOKEN_KEY.query.filter_by(**filter_token).first()

        #stap 2 check tokenKey ต้องมีใน Database
        if tokenCheck is None:
            response = {
                'status':False,
                'message':'Application-Key failed due to the following reason: invalid token. Confirm that the access token in the authorization header is valid.'
            }
            return jsonify(response), 403

        #step 3 ตัวสอบค่าที่ postมาต้องเป็น facultyid 
        if params_obj and \
            params_obj.get("facultyid") != None :
            
            # ถ้ามีข้อมูลให้ส่งเข้า db
            data_response = std.select_fac_by_filter(params_obj)
            timestampStr = int(time.time() * 1000)

            #step 4 ถ้าตรวจสอบใน db ผ่าน return เป็น Fac Info
            if  data_response:
                dt['logDescription'] = 'Get Data Faculty Student By filter Success'
                dt['logStaus'] = 'TRUE : 200 Method:' + request.method + '/Faculty Student By filter'
                
                log.add_log(dt)
                resp_obj = {
                    "timestamp": timestampStr,
                    "status": True,
                    "message": "Success",
                    "data": data_response
                }
                return jsonify(resp_obj)

            else:
                response = {
                    "timestamp": timestampStr,
                    "status":False,
                    "message":"Get Data Faculty Student is Empty!",
                    "data": None
                }
                return jsonify(response)
        else:
            #กรณีไม่ส่งค่า Faculty
            response = {
                'status':False,
                'message':'Faculty ID is Invalid!. Must follow the scheme,"/?facultyid={Fac ID}"'
            }
            return jsonify(response), 400

    else:
        response = {
            'status':False,
            'message':'Application-Key header required. Must follow the scheme, [Application-Key: <ACCESS TOKEN>]'
        }
        return jsonify(response), 401

# path : /api/v2/std/dep/all/
# Method : GET
# Description : Get all Department Student
# response : 
# {
#     "timestamp": 1580800196456,
#     "status": true,
#     "message": "Success",
#     "data": [
#         {
#             "facultyid": "12",
#             "depid": "1211",
#             "department": "สาขาวิชากายภาพบำบัด"
#         },
#         {
#             "facultyid": "6",
#             "depid": "0627",
#             "department": "ภาควิชาสเปนและลาตินอเมริกันศึกษา"
#         },...
#     ]
# }

@std_bp.route("/dep/all/")
def dep_all():

    # รับค่า header จาก request
    header_obj = request.headers

    #  silent=True = ให้ไม่ต้องแสดง error จาก การ get data json ได้
    body_obj = request.get_json(silent=True)

    # ประกาศ Object สำหรับเก็บค่า log
    dt = dict()

    #stap 1 check Application-Key ต้องไม่เท่ากับค่าว่าง
    if header_obj.get("Application-Key"):
        dt['tokenKey'] = header_obj.get("Application-Key")
        dt['logPage'] = request.path
        dt['logMethodName'] = 'Department Student All'

        # เตรียมข้อมูลที่ขอ Token Key
        filter_token = {
            "tokenKey":header_obj["Application-Key"],
            "tokenWebhookStatus":"E",
            "tokenClientType":"Public"
        }

        #stap 2 check tokenKey ต้องมีใน Database
        tokenCheck = token.SYS_TOKEN_KEY.query.filter_by(**filter_token).first()

        
        #กรณี Token ไม่ถูกจาก Database
        if tokenCheck is None:
            response = {
                'status':False,
                'message':'Application-Key failed due to the following reason: invalid token. Confirm that the access token in the authorization header is valid.'
            }
            return jsonify(response), 403

        #step 3 ค้นหา Fac or Dep from DB
        data_response = std.select_dep_by_filter()
        timestampStr = int(time.time() * 1000)

        if  data_response:
            dt['logDescription'] = 'Get Data Department Student All Success!'
            dt['logStaus'] = 'TRUE : 200 Method:' + request.method + '/Department Student All'
            
            log.add_log(dt)
            resp_obj = {
                "timestamp": timestampStr,
                "status": True,
                "message": "Success",
                "data": data_response
            }
            return jsonify(resp_obj)
        else:
            response = {
                "timestamp": timestampStr,
                "status": False,
                "message": "Get Data Department Student is Empty!",
                "data": None
            }
            return jsonify(response)
    else:
        #กรณีไม่ส่ง Token มา
        response = {
            'status':False,
            'message':'Application-Key header required. Must follow the scheme, [Application-Key: <ACCESS TOKEN>]'
        }
        return jsonify(response), 401

# path : /api/v2/std/dep/find?facultyid={facultyid}
# Method : GET
# Reqest : {facultyid:XXXX,depid:YYYY}
# Description : Get Department Student by facultyid, depid
# response : 
# {
#     "timestamp": 1580800435742,
#     "status": true,
#     "message": "Success",
#     "data": [
#         {
#             "facultyid": "12",
#             "depid": "1211",
#             "department": "สาขาวิชากายภาพบำบัด"
#         }
#     ]
# }

@std_bp.route("/dep/find/")
def dep_find():

    # รับค่า header จาก request
    header_obj = request.headers
    
    # Get request 'username'
    params_obj = request.args.to_dict()

    # ประกาศ Object สำหรับเก็บค่า log
    dt = dict()

    #stap 1 check Application-Key ต้องไม่เท่ากับค่าว่าง
    if header_obj.get("Application-Key"):
        dt['tokenKey'] = header_obj.get("Application-Key")
        dt['logPage'] = request.full_path
        dt['logMethodName'] = 'Department Student By filter'

        # เตรียมข้อมูลที่ขอ Token Key
        filter_token = {
            "tokenKey":header_obj["Application-Key"],
            "tokenWebhookStatus":"E",
            "tokenClientType":"Public"
        }

        #stap 2 check tokenKey ต้องมีใน Database
        tokenCheck = token.SYS_TOKEN_KEY.query.filter_by(**filter_token).first()

        # check tokenKey ต้องมีใน Database
        if tokenCheck is None:
            response = {
                'status':False,
                'message':'Application-Key failed due to the following reason: invalid token. Confirm that the access token in the authorization header is valid.'
            }
            return jsonify(response), 403


        #step 3 ตัวสอบค่าที่ get มาต้องเป็น facultyid หรือ depid
        if params_obj and \
            (params_obj.get("facultyid") != None or \
            params_obj.get("depid") != None):

            #step 4 ถ้าตรวจสอบใน db ผ่าน return เป็น Fac Info
            data_response = std.select_dep_by_filter(params_obj)
            #step 5 Get Timestamp
            timestampStr = int(time.time() * 1000)

            if data_response:
                dt['logDescription'] = 'Get Data Department Student By filter Success'
                dt['logStaus'] = 'TRUE : 200 Method:' + request.method + '/Department Student By filter'
                
                log.add_log(dt)
                resp_obj = {
                    "timestamp": timestampStr,
                    "status": True,
                    "message": "Success",
                    "data": data_response
                }
                return jsonify(resp_obj)
            else:

                response = {
                    "timestamp": timestampStr,
                    'status':False,
                    'message':'Get Data Department Student is Empty!',
                    "data": None
                }
                return jsonify(response)
        else:
            #กรณีส่งค่า username
            response = {
                'status':False,
                'message':'Department Student Invalid!. Must follow the scheme,"/find/?dep_code={dep_code}"'
            }
            return jsonify(response), 400

    else:
        response = {
            'status':False,
            'message':'Application-Key header required. Must follow the scheme, [Application-Key: <ACCESS TOKEN>]'
        }
        return jsonify(response), 401

