from flask import Blueprint,current_app,request,jsonify
from ..models import  token,log,masterdata
import time
import json

master_bp = Blueprint("master", __name__)

# path : /api/v2/master/location/map_thai?district=คลองหลวง
# Method : GET
# Description : Get all Organization
# response : 
# {
#     "timestamp": 1580800196456,
#     "status": true,
#     "message": "Success",
#     "data": [
#         {
#         "sub_district": "คลองสอง",
#         "district": "คลองหลวง",
#         "province": "ปทุมธานี",
#         "zipcode": 12120,
#         "sub_district_code": 130202,
#         "district_code": 1302,
#         "province_code": 13
#         },
#         {
#         "sub_district": "คลองสาม",
#         "district": "คลองหลวง",
#         "province": "ปทุมธานี",
#         "zipcode": 12120,
#         "sub_district_code": 130203,
#         "district_code": 1302,
#         "province_code": 13
#         },
#     ]
# }

@master_bp.route("/location/map_thai/")
def map_thai():

    # รับค่า header จาก request
    header_obj = request.headers

    # ประกาศ Object สำหรับเก็บค่า log
    dt = dict()

    # Get request params
    params_obj = request.args.to_dict()

    # Check Application-Key ต้องไม่เท่ากับค่าว่าง
    if header_obj.get("Application-Key"):
        dt['tokenKey'] = header_obj.get("Application-Key")
        dt['logPage'] = request.full_path
        dt['logMethodName'] = 'Map Thailand'

        # เตรียมข้อมูลที่ขอ Token Key
        filter_token = {
            "tokenKey":header_obj["Application-Key"],
            "tokenWebhookStatus":"E",
            "tokenClientType":"Public"
        }
        tokenCheck = token.SYS_TOKEN_KEY.query.filter_by(**filter_token).first()

        # check tokenKey ต้องมีใน Database
        if tokenCheck:
            data_response = masterdata.select_map_thai_by_filter(params_obj)
            timestampStr = int(time.time() * 1000)

            if data_response:
                dt['logDescription'] = 'Get Map Thailand Success!'
                dt['logStaus'] = 'TRUE : 200 Method:' + request.method + '/Map Thailand'
                
                # log.add_log(dt)
                resp_obj = {
                    "timestamp": timestampStr,
                    "status": True,
                    "message": "Success",
                    "data": data_response
                }
                return jsonify(resp_obj)

            else:
                response = {
                    "timestamp": timestampStr,
                    "status": False,
                    "message": "Get Map Thailand is Empty!",
                    "data": None
                }
                return jsonify(response)
        
        else:
            response = {
                'status':False,
                'message':'Application-Key failed due to the following reason: invalid token. Confirm that the access token in the authorization header is valid.'
            }
            return jsonify(response), 403

    else:
        response = {
            'status':False,
            'message':'Application-Key header required. Must follow the scheme, [Application-Key: <ACCESS TOKEN>]'
        }
        return jsonify(response), 401
