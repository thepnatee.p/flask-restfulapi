from flask import Blueprint,request,jsonify
from ..models import token,log,org
import time
import json

org_bp = Blueprint("org", __name__)

# path : /api/v2/org/flow/find/?username={username}
# Method : GET
# Reqest : {username:thep_p}
# Description : Validate Username from DB
# response : 
# {
#     "timestamp": 1580800029218,
#     "status": true,
#     "message": "Success",
#     "data": {
#         'Level': 'h2', 
#         'username': 'thep21', 
#         'childen': [
#             {
#                 'Level': 'h3', 
#                 'username': 'thep31', 
#                 'childen': [
#                     {
#                         'Level': 'h4', 
#                         'username': 'thep41'
#                     }
#                 ]
#             }, {
#                 'Level': 'h3',
#                 'username': 'thep32'
#             }
#         ]
#     }
# }

# @org_bp.route("/find/", methods=['POST']) ถ้าส่งเป็น Method POST

@org_bp.route("/flow/find/")
def flow_info():
    # รับค่า header จาก request
    header_obj = request.headers
    

    params_obj = request.args.to_dict()
    username = params_obj.get("username")
    code = params_obj.get("code")

    # ประกาศ Object สำหรับเก็บค่า log
    dt = dict()
    

    # Check Application-Key ต้องไม่เท่ากับค่าว่าง
    if header_obj.get("Application-Key"):
        dt['tokenKey'] = header_obj.get("Application-Key")
        dt['logPage'] = request.full_path
        dt['logMethodName'] = 'Flow Organization'

        # เตรียมข้อมูลที่ขอ Token Key
        filter_token = {
            "tokenKey":header_obj["Application-Key"],
            "tokenWebhookStatus":"E",
            "tokenClientType":"Public"
        }
        tokenCheck = token.SYS_TOKEN_KEY.query.filter_by(**filter_token).first()

        # check tokenKey ต้องมีใน Database
        if tokenCheck is None:
            response = {
                'status':False,
                'message':'Application-Key failed due to the following reason: invalid token. Confirm that the access token in the authorization header is valid.'
            }
            return jsonify(response), 403

        # Check Application-Key ต้องไม่เท่ากับค่าว่าง
        # ตัวสอบค่าที่ get มาต้องเป็น username 
        if username and code:
            filter_flow = {
                "token":header_obj["Application-Key"],
                "flowCode":code,
                "RECORD_STATUS":"N"
            }
            org_flow = org.ORG_FLOW.query.filter_by(**filter_flow).first().flowDataJson
            org_flow = json.loads(org_flow)

            data_response  = org.select_flow_by_filter(org_flow,username)
            timestampStr = int(time.time() * 1000)
            if  data_response :
                dt['logDescription'] = 'Get Flow Organization Success  : ' + username
                dt['logStaus'] = 'TRUE : 200 Method:' + request.method + '/Flow Organization'
                
                log.add_log(dt)
                resp_obj = {
                    "timestamp": timestampStr,
                    "status": True,
                    "message": "Success",
                    "data": data_response
                }
                return jsonify(resp_obj)

            else:
                #กรณี Username ไม่ถูก
                dt['logDescription'] = ' Username Invalid!'
                dt['logStaus'] = 'FALSE : 400 Method:' + request.method + '/Flow Organization'

                log.add_log(dt)
                
                response = {
                    "timestamp": timestampStr,
                    'status':False,
                    'message': username + 'Username Invalid!',
                    "data": None
                }
                return jsonify(response), 400

        else:
            #กรณีไม่ส่งค่า UserName
            response = {
                'status':False,
                'message':'Request Query String a GET operation (HTTP method)!,"?username={Username}&code={FlowCode}"'
            }
            return jsonify(response), 400

    else:
        response = {
            'status':False,
            'message':'Application-Key header required. Must follow the scheme, [Application-Key: <ACCESS TOKEN>]'
        }
        return jsonify(response), 401
