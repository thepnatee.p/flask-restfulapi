from flask import Blueprint,current_app,request,jsonify
from ..models import  token,log,report
import time
import json

report_bp = Blueprint("report", __name__)

# path : /api/v2/report/std/fac/all
# Method : GET
# Description : Get Count all Faculty Student
# response : 
# {
#     "timestamp": 1580800196456,
#     "status": true,
#     "message": "Success",
#     "data": [
#         {
#             "faculty_name_th": "คณะพาณิชยศาสตร์และการบัญชี",
#             "faculty_name_en": "Thammasat Business School",
#             "counts": 8629
#         },
#         {
#             "faculty_name_th": "คณะนิติศาสตร์",
#             "faculty_name_en": "Faculty of Law",
#             "counts": 7112
#         },...
#     ]
# }

@report_bp.route("/std/fac/all/")
def fac_all():
    # รับค่า header จาก request
    header_obj = request.headers
    # ประกาศ Object สำหรับเก็บค่า log
    dt = dict()
    # Check Application-Key ต้องไม่เท่ากับค่าว่าง
    if header_obj.get("Application-Key"):
        dt['tokenKey'] = header_obj.get("Application-Key")
        dt['logPage'] = request.path
        dt['logMethodName'] = 'Faculty Student'

        # เตรียมข้อมูลที่ขอ Token Key
        filter_token = {
            "tokenKey":header_obj["Application-Key"],
            "tokenWebhookStatus":"E",
            "tokenClientType":"Public"
        }
        tokenCheck = token.SYS_TOKEN_KEY.query.filter_by(**filter_token).first()

        # check tokenKey ต้องมีใน Database
        if tokenCheck:
            data_response = report.select_std_fac_filter()
            timestampStr = int(time.time() * 1000)

            if data_response:
                dt['logDescription'] = 'Get Report Data of Faculty Success!'
                dt['logStaus'] = 'TRUE : 200 Method:' + request.method + '/Report Faculty All'
                
                log.add_log(dt)
                resp_obj = {
                    "timestamp": timestampStr,
                    "status": True,
                    "message": "Success",
                    "data": data_response
                }
                return jsonify(resp_obj)

            else:
                response = {
                    "timestamp": timestampStr,
                    "status": False,
                    "message": "GGet Report Data of Faculty is Empty!",
                    "data": None
                }
                return jsonify(response)
        
        else:
            response = {
                'status':False,
                'message':'Application-Key failed due to the following reason: invalid token. Confirm that the access token in the authorization header is valid.'
            }
            return jsonify(response), 403
    else:
        response = {
            'status':False,
            'message':'Application-Key header required. Must follow the scheme, [Application-Key: <ACCESS TOKEN>]'
        }
        return jsonify(response), 401


@report_bp.route("/std/fac/find/")
def fac_find():

    # รับค่า header จาก request
    header_obj = request.headers

    #  silent=True = ให้ไม่ต้องแสดง error จาก การ get data json ได้
    body_obj = request.get_json(silent=True)

    # Get request 'username'
    params_obj = request.args.to_dict()

    # ประกาศ Object สำหรับเก็บค่า log
    dt = dict()

    # Check Application-Key ต้องไม่เท่ากับค่าว่าง
    if header_obj.get("Application-Key"):
        dt['tokenKey'] = header_obj.get("Application-Key")
        dt['logPage'] = request.full_path
        dt['logMethodName'] = 'Organization'

        # เตรียมข้อมูลที่ขอ Token Key
        filter_token = {
            "tokenKey":header_obj["Application-Key"],
            "tokenWebhookStatus":"E",
            "tokenClientType":"Public"
        }
        tokenCheck = token.SYS_TOKEN_KEY.query.filter_by(**filter_token).first()

        # check tokenKey ต้องมีใน Database
        if tokenCheck is None:
            response = {
                'status':False,
                'message':'Application-Key failed due to the following reason: invalid token. Confirm that the access token in the authorization header is valid.'
            }
            return jsonify(response), 403

        # Check Application-Key ต้องไม่เท่ากับค่าว่าง
        # ตัวสอบค่าที่ get มาต้องเป็น username 
        if params_obj and \
            (params_obj.get("faculty_name_th") != None or \
            params_obj.get("faculty_name_en") != None):
            # ถ้ามีข้อมูลให้ส่งเข้า db
            data_response = report.select_std_fac_filter(params_obj)
            timestampStr = int(time.time() * 1000)

            if data_response:
                dt['logDescription'] = 'Get Report Data of Faculty By filter Success'
                dt['logStaus'] = 'TRUE : 200 Method:' + request.method + '/Organization By filter'
                
                log.add_log(dt)
                resp_obj = {
                    "timestamp": timestampStr,
                    "status": True,
                    "message": "Success",
                    "data": data_response
                }
                return jsonify(resp_obj)

            else:
                response = {
                    "timestamp": timestampStr,
                    'status':False,
                    'message':'Get Report Data of Faculty is Empty!',
                    "data": None
                }
                return jsonify(response)
        else:
            #กรณีส่งค่า username
            response = {
                'status':False,
                'message':'Organization Invalid!. Must follow the scheme,"/find/?faculty_name_th={faculty_name_th}&faculty_name_en={faculty_name_th}"'
            }
            return jsonify(response), 400

    else:
        response = {
            'status':False,
            'message':'Application-Key header required. Must follow the scheme, [Application-Key: <ACCESS TOKEN>]'
        }
        return jsonify(response), 401

# path : /api/v2/report/std/level/all/
# Method : GET
# Description : Get Count all Level Student
# response : 
# {
#     "timestamp": 1580800196456,
#     "status": true,
#     "message": "Success",
#     "data": [
#         {
#             "level_name": "ปริญญาตรี",
#             "counts": 8629
#         },
#         {
#             "level_name": "ปริญญาโท",
#             "counts": 7112
#         },...
#     ]
# }

@report_bp.route("/std/level/all/")
def level_all():

    # รับค่า header จาก request
    header_obj = request.headers

    # ประกาศ Object สำหรับเก็บค่า log
    dt = dict()

    # Check Application-Key ต้องไม่เท่ากับค่าว่าง
    if header_obj.get("Application-Key"):
        dt['tokenKey'] = header_obj.get("Application-Key")
        dt['logPage'] = request.path
        dt['logMethodName'] = 'Level Student'

        # เตรียมข้อมูลที่ขอ Token Key
        filter_token = {
            "tokenKey":header_obj["Application-Key"],
            "tokenWebhookStatus":"E",
            "tokenClientType":"Public"
        }
        tokenCheck = token.SYS_TOKEN_KEY.query.filter_by(**filter_token).first()

        # check tokenKey ต้องมีใน Database
        if tokenCheck:
            data_response = report.select_std_level_filter()
            timestampStr = int(time.time() * 1000)

            if data_response:
                dt['logDescription'] = 'Get Report Data of Level Success!'
                dt['logStaus'] = 'TRUE : 200 Method:' + request.method + '/Report Level All'
                
                log.add_log(dt)
                resp_obj = {
                    "timestamp": timestampStr,
                    "status": True,
                    "message": "Success",
                    "data": data_response
                }
                return jsonify(resp_obj)

            else:
                response = {
                    "timestamp": timestampStr,
                    "status": False,
                    "message": "Get Report Data of Level is Empty!",
                    "data": None
                }
                return jsonify(response)
        
        else:
            response = {
                'status':False,
                'message':'Application-Key failed due to the following reason: invalid token. Confirm that the access token in the authorization header is valid.'
            }
            return jsonify(response), 403

    else:
        response = {
            'status':False,
            'message':'Application-Key header required. Must follow the scheme, [Application-Key: <ACCESS TOKEN>]'
        }
        return jsonify(response), 401

# path : /api/v2/report/status/all/
# Method : GET
# Description : Get Count all Status Student
# response : 
# {
#     "timestamp": 1580800196456,
#     "status": true,
#     "message": "Success",
#     "data": [
#         {
#             "status_name": "จบการศึกษา",
#             "counts": 8629
#         },
#         {
#             "status_name": "ปกติ",
#             "counts": 7112
#         },...
#     ]
# }

@report_bp.route("/std/status/all/")
def status_all():

    # รับค่า header จาก request
    header_obj = request.headers

    # ประกาศ Object สำหรับเก็บค่า log
    dt = dict()

    # Check Application-Key ต้องไม่เท่ากับค่าว่าง
    if header_obj.get("Application-Key"):
        dt['tokenKey'] = header_obj.get("Application-Key")
        dt['logPage'] = request.path
        dt['logMethodName'] = 'Status Student'

        # เตรียมข้อมูลที่ขอ Token Key
        filter_token = {
            "tokenKey":header_obj["Application-Key"],
            "tokenWebhookStatus":"E",
            "tokenClientType":"Public"
        }
        tokenCheck = token.SYS_TOKEN_KEY.query.filter_by(**filter_token).first()

        # check tokenKey ต้องมีใน Database
        if tokenCheck:
            data_response = report.select_std_status_filter()
            timestampStr = int(time.time() * 1000)

            if data_response:
                dt['logDescription'] = 'Get Report Data of Level Success!'
                dt['logStaus'] = 'TRUE : 200 Method:' + request.method + '/Report Status All'
                
                log.add_log(dt)
                resp_obj = {
                    "timestamp": timestampStr,
                    "status": True,
                    "message": "Success",
                    "data": data_response
                }
                return jsonify(resp_obj)

            else:
                response = {
                    "timestamp": timestampStr,
                    "status": False,
                    "message": "Get Report Data of Status is Empty!",
                    "data": None
                }
                return jsonify(response)
        
        else:
            response = {
                'status':False,
                'message':'Application-Key failed due to the following reason: invalid token. Confirm that the access token in the authorization header is valid.'
            }
            return jsonify(response), 403

    else:
        response = {
            'status':False,
            'message':'Application-Key header required. Must follow the scheme, [Application-Key: <ACCESS TOKEN>]'
        }
        return jsonify(response), 401


# path : /api/v2/report/std/Adyear/all
# Method : GET
# Description : Get Count all Admission Year (Adyear) Student
# response : 
# {
#     "timestamp": 1580800196456,
#     "status": true,
#     "message": "Success",
#     "data": [       
#               {
#                   "adyaer": "63",
#                   "counts": null
#               },
#               {
#                    "adyaer": "62",
#                     "counts": 11683
#               },...
#     ]
# }

@report_bp.route("/std/Adyear/all/")
def adyear_all():
    # รับค่า header จาก request
    header_obj = request.headers
    # ประกาศ Object สำหรับเก็บค่า log
    dt = dict()
    # Check Application-Key ต้องไม่เท่ากับค่าว่าง
    if header_obj.get("Application-Key"):
        dt['tokenKey'] = header_obj.get("Application-Key")
        dt['logPage'] = request.path
        dt['logMethodName'] = 'Adyear Student'

        # เตรียมข้อมูลที่ขอ Token Key
        filter_token = {
            "tokenKey":header_obj["Application-Key"],
            "tokenWebhookStatus":"E",
            "tokenClientType":"Public"
        }
        tokenCheck = token.SYS_TOKEN_KEY.query.filter_by(**filter_token).first()

        # check tokenKey ต้องมีใน Database
        if tokenCheck:
            data_response = report.select_std_adyear_filter()
            timestampStr = int(time.time() * 1000)

            if data_response:
                dt['logDescription'] = 'Get Report Data of Adyear Success!'
                dt['logStaus'] = 'TRUE : 200 Method:' + request.method + '/Report Adyear All'
                
                log.add_log(dt)
                resp_obj = {
                    "timestamp": timestampStr,
                    "status": True,
                    "message": "Success",
                    "data": data_response
                }
                return jsonify(resp_obj)

            else:
                response = {
                    "timestamp": timestampStr,
                    "status": False,
                    "message": "Get Report Data of Adyear is Empty!",
                    "data": None
                }
                return jsonify(response)
        
        else:
            response = {
                'status':False,
                'message':'Application-Key failed due to the following reason: invalid token. Confirm that the access token in the authorization header is valid.'
            }
            return jsonify(response), 403
    else:
        response = {
            'status':False,
            'message':'Application-Key header required. Must follow the scheme, [Application-Key: <ACCESS TOKEN>]'
        }
        return jsonify(response), 401


# path : /api/v2/report/std/Bucket/all
# Method : GET
# Description : Get Count all Admission Year (Adyear) Student
# response : 
# {
#     "timestamp": 1580800196456,
#     "status": true,
#     "message": "Success",
#     "data": [       
#               {
#                   "faculty_name_th": "คณะทันตแพทยศาสตร์",
#                   "faculty_name_en": "Faculty of Dentistry",
#                   "adyear": "50",
#                   "level_name": "ปริญญาตรี",
#                   "counts": 72
#                },
#                {
#                   "faculty_name_th": "คณะทันตแพทยศาสตร์",
#                   "faculty_name_en": "Faculty of Dentistry",
#                   "adyear": "50",
#                   "level_name": "ปริญญาเอก",
#                   "counts": 1
#               },...
#     ]
# }

@report_bp.route("/std/Bucket/all/")
def bucket_all():
    # รับค่า header จาก request
    header_obj = request.headers
    # ประกาศ Object สำหรับเก็บค่า log
    dt = dict()
    # Check Application-Key ต้องไม่เท่ากับค่าว่าง
    if header_obj.get("Application-Key"):
        dt['tokenKey'] = header_obj.get("Application-Key")
        dt['logPage'] = request.path
        dt['logMethodName'] = 'Bucket Student'

        # เตรียมข้อมูลที่ขอ Token Key
        filter_token = {
            "tokenKey":header_obj["Application-Key"],
            "tokenWebhookStatus":"E",
            "tokenClientType":"Public"
        }
        tokenCheck = token.SYS_TOKEN_KEY.query.filter_by(**filter_token).first()

        # check tokenKey ต้องมีใน Database
        if tokenCheck:
            data_response = report.select_std_bucket_filter()
            timestampStr = int(time.time() * 1000)

            if data_response:
                dt['logDescription'] = 'Get Report Data of Bucket Success!'
                dt['logStaus'] = 'TRUE : 200 Method:' + request.method + '/Report Bucket All'
                
                log.add_log(dt)
                resp_obj = {
                    "timestamp": timestampStr,
                    "status": True,
                    "message": "Success",
                    "data": data_response
                }
                return jsonify(resp_obj)

            else:

                response = {
                    "timestamp": timestampStr,
                    "status": False,
                    "message": "Get Report Data of Bucket is Empty!",
                    "data": None
                }
                return jsonify(response)
        
        else:
            response = {
                'status':False,
                'message':'Application-Key failed due to the following reason: invalid token. Confirm that the access token in the authorization header is valid.'
            }
            return jsonify(response), 403
    else:
        response = {
            'status':False,
            'message':'Application-Key header required. Must follow the scheme, [Application-Key: <ACCESS TOKEN>]'
        }
        return jsonify(response), 401


@report_bp.route("/std/Bucket/find/")
def bucket_find():

    # รับค่า header จาก request
    header_obj = request.headers

    #  silent=True = ให้ไม่ต้องแสดง error จาก การ get data json ได้
    body_obj = request.get_json(silent=True)

    # Get request 'username'
    params_obj = request.args.to_dict()

    # ประกาศ Object สำหรับเก็บค่า log
    dt = dict()

    # Check Application-Key ต้องไม่เท่ากับค่าว่าง
    if header_obj.get("Application-Key"):
        dt['tokenKey'] = header_obj.get("Application-Key")
        dt['logPage'] = request.full_path
        dt['logMethodName'] = 'Bucket Student'

        # เตรียมข้อมูลที่ขอ Token Key
        filter_token = {
            "tokenKey":header_obj["Application-Key"],
            "tokenWebhookStatus":"E",
            "tokenClientType":"Public"
        }
        tokenCheck = token.SYS_TOKEN_KEY.query.filter_by(**filter_token).first()

        # check tokenKey ต้องมีใน Database
        if tokenCheck is None:
            response = {
                'status':False,
                'message':'Application-Key failed due to the following reason: invalid token. Confirm that the access token in the authorization header is valid.'
            }
            return jsonify(response), 403

        # Check Application-Key ต้องไม่เท่ากับค่าว่าง
        # ตัวสอบค่าที่ get มาต้องเป็น username 
        if params_obj and \
            ( params_obj.get("faculty_name_th") != None or \
              params_obj.get("faculty_name_en") != None or\
              params_obj.get("adyear") != None or \
              params_obj.get("level_name") != None):
            # ถ้ามีข้อมูลให้ส่งเข้า db
            data_response = report.select_std_bucket_filter(params_obj)
            timestampStr = int(time.time() * 1000)

            if data_response:
                dt['logDescription'] = 'Get Report Data of Bucket  by filter Success'
                dt['logStaus'] = 'TRUE : 200 Method:' + request.method + '/Organization By filter'
                
                log.add_log(dt)
                resp_obj = {
                    "timestamp": timestampStr,
                    "status": True,
                    "message": "Success",
                    "data": data_response
                }
                return jsonify(resp_obj)

            else:
                
                response = {
                    "timestamp": timestampStr,
                    'status':False,
                    'message':'Get Report Data of Bucket is Empty!',
                    "data": None
                }
                return jsonify(response)
        else:
            #กรณีส่งค่า username
            response = {
                'status':False,
                'message':'Organization Invalid!. Must follow the scheme,"/find/?faculty_name_th={faculty_name_th}&faculty_name_en={faculty_name_th}"'
            }
            return jsonify(response), 400

    else:
        response = {
            'status':False,
            'message':'Application-Key header required. Must follow the scheme, [Application-Key: <ACCESS TOKEN>]'
        }
        return jsonify(response), 401

@report_bp.route("/emp/org/all/")
def emp_org_all():

    # รับค่า header จาก request
    header_obj = request.headers
    # ประกาศ Object สำหรับเก็บค่า log
    dt = dict()
    # Check Application-Key ต้องไม่เท่ากับค่าว่าง
    if header_obj.get("Application-Key"):
        dt['tokenKey'] = header_obj.get("Application-Key")
        dt['logPage'] = request.path
        dt['logMethodName'] = 'Bucket Student'

        # เตรียมข้อมูลที่ขอ Token Key
        filter_token = {
            "tokenKey":header_obj["Application-Key"],
            "tokenWebhookStatus":"E",
            "tokenClientType":"Public"
        }
        tokenCheck = token.SYS_TOKEN_KEY.query.filter_by(**filter_token).first()

        # check tokenKey ต้องมีใน Database
        if tokenCheck:
            data_response = report.select_emp_org_filter()
            timestampStr = int(time.time() * 1000)

            if data_response:
                dt['logDescription'] = 'Get Report Data of Bucket Success!'
                dt['logStaus'] = 'TRUE : 200 Method:' + request.method + '/Report Bucket All'
                
                log.add_log(dt)
                resp_obj = {
                    "timestamp": timestampStr,
                    "status": True,
                    "message": "Success",
                    "data": data_response
                }
                return jsonify(resp_obj)

            else:

                response = {
                    "timestamp": timestampStr,
                    "status": False,
                    "message": "Get Report Data of Bucket is Empty!",
                    "data": None
                }
                return jsonify(response)
        
        else:
            response = {
                'status':False,
                'message':'Application-Key failed due to the following reason: invalid token. Confirm that the access token in the authorization header is valid.'
            }
            return jsonify(response), 403
    else:
        response = {
            'status':False,
            'message':'Application-Key header required. Must follow the scheme, [Application-Key: <ACCESS TOKEN>]'
        }
        return jsonify(response), 401

