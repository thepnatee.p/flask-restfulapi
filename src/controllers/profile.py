from flask import Blueprint,request,jsonify
from ..models import token,log,emp,std
import time
import json

profile_bp = Blueprint("profile", __name__)

# path : /api/v2/profile/emp/info/?username={username}
# Method : GET
# Reqest : {username:XXXX}
# Description : Validate Username from DB
# response : 
# {
#     "timestamp": 1580800029218,
#     "status": true,
#     "message": "Success",
#     "data": {
#         "displayname_th": "ศุภกาญจน์  ประดุจกาญจนา",
#         "displayname_en": "Suphakarn  Pradujkanchana",
#         "employee_type": "พนักงานมหาวิทยาลัย",
#         "organization": "สำนักงานศูนย์เทคโนโลยีสารสนเทศและการสื่อสาร",
#         "department": "งานวิเคราะห์และพัฒนาระบบ"
#     }
# }

# @profile_bp.route("/emp/info/", methods=['POST']) ถ้าส่งเป็น Method POST

@profile_bp.route("/emp/info/")
def emp_info():
    # รับค่า header จาก request
    header_obj = request.headers
    
    username = request.args.get('username')

    # ประกาศ Object สำหรับเก็บค่า log
    dt = dict()
    

    # Check Application-Key ต้องไม่เท่ากับค่าว่าง
    if header_obj.get("Application-Key"):
        dt['tokenKey'] = header_obj.get("Application-Key")
        dt['logPage'] = request.full_path
        dt['logMethodName'] = 'Profile Employee'

        # เตรียมข้อมูลที่ขอ Token Key
        filter_token = {
            "tokenKey":header_obj["Application-Key"],
            "tokenWebhookStatus":"E",
            "tokenClientType":"Public"
        }
        tokenCheck = token.SYS_TOKEN_KEY.query.filter_by(**filter_token).first()

        # check tokenKey ต้องมีใน Database
        if tokenCheck is None:
            response = {
                'status':False,
                'message':'Application-Key failed due to the following reason: invalid token. Confirm that the access token in the authorization header is valid.'
            }
            return jsonify(response), 403

        # Check Application-Key ต้องไม่เท่ากับค่าว่าง
        # ตัวสอบค่าที่ get มาต้องเป็น username 
        if username:
            # ถ้ามีข้อมูลให้ส่งเข้า db
            data_response  = emp.select_profile_by_filter(username)
            timestampStr = int(time.time() * 1000)
            if  data_response :
                dt['logDescription'] = 'Get Profile Employee Success  : ' + username
                dt['logStaus'] = 'TRUE : 200 Method:' + request.method + '/Profile Employee'
                
                log.add_log(dt)
                resp_obj = {
                    "timestamp": timestampStr,
                    "status": True,
                    "message": "Success",
                    "data":  {
                        'displayname_th' : data_response['displayname_th'],
                        'displayname_en' : data_response['displayname_en'],
                        'employee_type' : data_response['employee_type'],
                        'organization' : data_response['organization'],
                        'department' : data_response['department'],
                    } 
                }
                return jsonify(resp_obj)

            else:
                #กรณี Username ไม่ถูก
                dt['logDescription'] = 'Username Invalid!'
                dt['logStaus'] = 'FALSE : 400 Method:' + request.method + '/Profile Employee'

                log.add_log(dt)
                
                response = {
                    "timestamp": timestampStr,
                    'status':False,
                    'message':'Username Invalid!',
                    "data": None
                }
                return jsonify(response), 400
        else:
            #กรณีไม่ส่งค่า UserName
            response = {
                'status':False,
                'message':'Request Query String a GET operation (HTTP method)!,"?username={Username}"'
            }
            return jsonify(response), 400

    else:
        response = {
            'status':False,
            'message':'Application-Key header required. Must follow the scheme, [Application-Key: <ACCESS TOKEN>]'
        }
        return jsonify(response), 401


# path : /api/v2/profile/std/info/?id={student id}
# Method : GET
# Reqest : {studnetId:XXXX}
# Description : Validate studnetId from DB
# response : 
# {
#     "timestamp": 1580799943012,
#     "status": true,
#     "message": "Success",
#     "data": {
#         "statusname": "หมดระยะเวลาการศึกษา(ต่างชาติ/เรียนระยะสั้น)",
#         "displayname_th": "เฌองาม ลิ้มประเสริฐ",
#         "displayname_en": "CHERNGAM LIMPRASERT",
#         "department": "คณะเศรษฐศาสตร์",
#         "faculty": "คณะเศรษฐศาสตร์"
#     }
# }

@profile_bp.route("/std/info/")
def std_info():
    # รับค่า header จาก request
    header_obj = request.headers
   
    studnetId = request.args.get('id')

    # ประกาศ Object สำหรับเก็บค่า log
    dt = dict()

    # Check Application-Key ต้องไม่เท่ากับค่าว่าง
    if header_obj.get("Application-Key"):
        dt['tokenKey'] = header_obj.get("Application-Key")
        dt['logPage'] = request.full_path
        dt['logMethodName'] = 'Profile Student'

        # เตรียมข้อมูลที่ขอ Token Key
        filter_token = {
            "tokenKey":header_obj["Application-Key"],
            "tokenWebhookStatus":"E",
            "tokenClientType":"Public"
        }
        tokenCheck = token.SYS_TOKEN_KEY.query.filter_by(**filter_token).first()

        # check tokenKey ต้องมีใน Database
        if tokenCheck is None:
            response = {
                'status':False,
                'message':'Application-Key failed due to the following reason: invalid token. Confirm that the access token in the authorization header is valid.'
            }
            return jsonify(response), 403

        # Check Application-Key ต้องไม่เท่ากับค่าว่าง
        # ตัวสอบค่าที่ get มาต้องเป็น id 
        if studnetId:
            # ถ้ามีข้อมูลให้ส่งเข้า db
            data_response  = std.select_profile_by_filter(studnetId)
            timestampStr = int(time.time() * 1000)

            if  data_response :
                dt['logDescription'] = 'Get Profile Student Success  : ' + studnetId
                dt['logStaus'] = 'TRUE : 200 Method:' + request.method + '/Profile Student'
                
                log.add_log(dt)
                res_logProfile = log.add_logProfile(data_response,studnetId,dt['tokenKey'])

                if res_logProfile is None :
                    print("Can't insert log profile")

                resp_obj = {
                    "timestamp": timestampStr,
                    "status": True,
                    "message": "Success",
                    "data":{
                        'statusname' : data_response['statusname'],
                        'displayname_th' : data_response['displayname_th'],
                        'displayname_en' : data_response['displayname_en'],
                        'level_name' : data_response['level_name'],
                        'faculty' : data_response['faculty'],
                        'department' : data_response['department']
                    }  

                }

                return jsonify(resp_obj)
            else:
                #กรณี studnetId ไม่ถูก
                dt['logDescription'] = 'Studnet ID Invalid!'
                dt['logStaus'] = 'FALSE : 400 Method:' + request.method + '/Profile Student'

                log.add_log(dt)
                
                response = {
                    "timestamp": timestampStr,
                    'status':False,
                    'message':'Studnet ID Invalid!',
                    "data": None
                }
                return jsonify(response), 400
        else:
            #กรณีไม่ส่งค่า Studnet ID
            response = {
                'status':False,
                'message':'Request Query String a GET operation (HTTP method)!,"?id={Student ID}"'
            }
            return jsonify(response), 400

    else:
        response = {
            'status':False,
            'message':'Application-Key header required. Must follow the scheme, [Application-Key: <ACCESS TOKEN>]'
        }
        return jsonify(response), 401