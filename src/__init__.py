from flask import Flask,request,render_template
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
import ssl

context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
context.verify_mode = ssl.CERT_REQUIRED
context.load_verify_locations("src/ssl/SectigoRSADomainValidationSecureServerCA.crt")
context.load_cert_chain('src/ssl/256597399.crt', 'src/ssl/server.key')

def create_app():
    # สร้าง app (core server) ภายในไฟล์
    app = Flask(__name__)

    # Config app
    app.config["JSON_SORT_KEYS"] = False

    limiter = Limiter(
        app,
        key_func=get_remote_address,

        # set default limit 
        # example set
        # default_limits=["2 per minute", "1 per second"]
        # default_limits=["1000 per day", "1 per second"],
        default_limits=["3000 per day"],
    )
    # Binds the application only
    with app.app_context():

        # import file bluprint ex. emp.py

        from .controllers import emp,profile,std,report,master,org

        # emp.emp_bp from emp.py 
        app.register_blueprint(emp.emp_bp, url_prefix="/api/v2/emp")
        app.register_blueprint(std.std_bp, url_prefix="/api/v2/std")
        app.register_blueprint(profile.profile_bp, url_prefix="/api/v2/profile")
        
        app.register_blueprint(report.report_bp, url_prefix="/api/v2/report")
        app.register_blueprint(master.master_bp, url_prefix="/api/v2/master")
        app.register_blueprint(org.org_bp, url_prefix="/api/v2/org")
    # set route path 
    @app.route('/')
    def index():
        return render_template("index.htm")

    return app